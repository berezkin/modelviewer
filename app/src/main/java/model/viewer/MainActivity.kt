package model.viewer

import android.animation.Animator
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.SkeletonNode
import com.google.ar.sceneform.animation.ModelAnimator
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.ArFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val assets = assets.list("models")
        debugInfo.text = "models: ${assets?.joinToString()}"
        var selected = assets?.firstOrNull()
        assets?.forEach { name ->
            val button = RadioButton(this)
            button.text = name
            modelsRadio.addView(button)
            button.setOnClickListener { selected = name }
        }

        var renderables = emptyList<ModelRenderable>()

        val fragment = arFragment as ArFragment
        fragment.setOnTapArPlaneListener { hitResult, plane, motionEvent ->
            selected?.let { name ->
                val anchor = hitResult.createAnchor()
                val anchorNode = AnchorNode(anchor)
                anchorNode.setParent(fragment.arSceneView.scene)
                val skeletonNode = SkeletonNode()
                skeletonNode.setParent(anchorNode)
                val uri = Uri.parse("models/$name")
                Log.d("XXX", "$uri")

                val future =
                    ModelRenderable.builder()
                        .setSource(this, uri)
                        .build()
                        .thenApply { renderable: ModelRenderable ->
                            skeletonNode.renderable = renderable
//                        node = skeletonNode
                            renderables += renderable
                            val text = (0 until renderable.animationDataCount).map {
                                renderable.getAnimationData(it)
                                    .let { "${it.name}:${it.durationMs}ms" }
                            }.joinToString()
                            debugInfo.text = "animations: $text"
                            startAnimation.isEnabled = true

                        }.exceptionally { throwable: Throwable? ->
                            debugInfo.text = "Error $throwable"
                            Log.e("XXX", "Error loading renderable", throwable)
                        }
            }
        }

        startAnimation.setOnClickListener {
            renderables.forEach { renderable ->
                Log.d("XXX", "count ${renderable.animationDataCount}")
                try {
                    if (renderable.animationDataCount > 0) {
                        val data = renderable.getAnimationData(0)
                        val animator = ModelAnimator(data, renderable)
                        animator.start()
                        debugInfo.text = "start animation"
                        animator.addListener(object : Animator.AnimatorListener {
                            override fun onAnimationRepeat(animation: Animator?) {
                            }

                            override fun onAnimationEnd(animation: Animator?) {
                                debugInfo.text = "animation end"
                            }

                            override fun onAnimationCancel(animation: Animator?) {
                                debugInfo.text = "animation cancel"
                            }

                            override fun onAnimationStart(animation: Animator?) {
                                debugInfo.text = "animation start"
                            }
                        })
                    }
                } catch (e: Exception) {
                    Log.d("XXX", "Animation already started", e)
                }
            }
        }
    }
}
